// Simple CLI Note taking application
package main

import (
	"fmt"
	"os"
	"slices"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/asukiasyan/notes-cli/backend"
)

func usage() {
	text := `

Please provide CLI arguments. 
Usage:
	note <argument> [body]
	Args:
		-l --list    - list all notes
		-a --add     - add a note
		-d --delete  - delete a note
		-e --edit    - edit the note
	Examples:
		note -l
		note -a "Some note"
		note -d 3 // where 3 is the ID of the note
		note -e 3 "New note to replace"

`
	fmt.Println(text)

}

func main() {

	args := os.Args[1:]
	validArgs := []string{"-l", "--list", "-a", "--add", "-d", "--delete", "-e", "--edit"}

	if len(args) == 0 || !slices.Contains(validArgs, args[0]) {
		usage()
		return
	}

	backend.CreateDB()

	if args[0] == "-l" || args[0] == "--list" {
		backend.ListAllNotes()
	} else if args[0] == "-a" || args[0] == "--add" {
		backend.AddNewNote(args[1])
	} else if args[0] == "-d" || args[0] == "--delete" {
		backend.DeleteNote(args[1])
	} else if args[0] == "-e" || args[0] == "--edit" {
		backend.UpdatewNote(args[2], args[1])
	} else {
		fmt.Println(os.Args[1])
	}

}
