package backend

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"os"
)

// CreateDB - Create new database if not exist
func CreateDB() {
	if _, err := os.Stat("./notes-data.db"); errors.Is(err, os.ErrNotExist) {
		os.Create("./notes-data.db")

		db, err := sql.Open("sqlite3", "./notes-data.db")
		if err != nil {
			log.Fatal(err)
			os.Exit(1)
		}

		_, err = db.Exec("CREATE TABLE `notes` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `body` VARCHAR(255) NOT NULL)")
		if err != nil {
			log.Fatal(err)
			os.Exit(1)
		}

		defer db.Close()

	}
}

// ListAllNotes - List All Notes
func ListAllNotes() {
	db, err := sql.Open("sqlite3", "./notes-data.db")
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	output, err := db.Query("SELECT * from notes")
	if err != nil {
		log.Fatal(err)
	}
	for output.Next() {
		var id int
		var body string
		err = output.Scan(&id, &body)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(id, body)
	}
	defer db.Close()
}

// AddNewNote - Add New Note
func AddNewNote(note string) {
	db, err := sql.Open("sqlite3", "./notes-data.db")
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	defer db.Close()

	stmt, err := db.Prepare("INSERT INTO notes VALUES(NULL, ?)")
	if err != nil {
		log.Fatal(err)
	}
	stmt.Exec(note)
	if err != nil {
		panic(err)
	}
}

// UpdatewNote - Update Note
func UpdatewNote(note string, id string) {
	db, err := sql.Open("sqlite3", "./notes-data.db")
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	defer db.Close()
	stmt, err := db.Prepare("UPDATE notes SET body = ? WHERE id = ?")
	stmt.Exec(note, id)
	if err != nil {
		panic(err)
	}
}

// DeleteNote - Delete Note
func DeleteNote(noteID string) {
	db, err := sql.Open("sqlite3", "./notes-data.db")
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	defer db.Close()

	stmt, err := db.Prepare("DELETE FROM notes WHERE id=?")
	if err != nil {
		log.Fatal(err)
	}
	stmt.Exec(noteID)
	if err != nil {
		panic(err)
	}
}
