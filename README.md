## Go Notes - Small CLI application for tracking your notes and snippets

### Basic command line options

    -l --list    - list all notes
    -a --add     - add a note
    -d --delete  - delete a note
    -e --edit    - edit the note

Examples:

    note -l
    note -a "Some note"
    note -d 3 // where 3 is the ID of the note
    note -e 3 "New note to replace"
